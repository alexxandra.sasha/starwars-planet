import React from 'react';
import "./item-details.css";
import ErrorButton from "../error-button";

const ItemDetails = (props) => {

  const {item, children: renderItem, image} = props;
  const {name} = item;
    return(
      <div className="person-details"> 
       <img src = {image} alt=''
      /* src={`http://starwars-visualguide.com/assets/img/characters/${id}.jpg`} */ 
            className="person-img"/>
       <div>
         <div className="person-name">
           {name}
         </div>
         <div className="options-person">
          {React.Children.map(/* this.props.children */ renderItem, (child ) => {
            return React.cloneElement(child, {item})
          })}
         </div>
       </div>
        <ErrorButton/>
      </div>
    );
  }

export default ItemDetails;