import React, {Component} from 'react';
import PropTypes from "prop-types";
import "./random-planet.css";
import SwapiService from "../../services/swapi-service";
import Loader from "../loader";
import Error from "../error";

export default class RandomPlanet extends Component {
  static defaultProps = {
    updateItnerval: 10000
  }
  static propTypes = {
    updateItnerval: PropTypes.number
  }

  swapiService = new SwapiService();
  
  state = {
    planet: {},
    loading: true,
    close: false,
  };

  componentDidMount () {
    const {updateItnerval}=this.props;
    this.updatePlanet();
    this.interval = setInterval(this.updatePlanet, updateItnerval);
  }

  onPlanetLoaded=(planet)=>{
    this.setState({planet,
                   loading: false,
                  error: false,
                  })
  }

  onError=(err)=>{
    this.setState({
      error: true,
      loading:false}) 
  }

  updatePlanet = () => {
    console.log("update")
    const id = Math.floor(Math.random()*17)+3;

    this.swapiService
      .getPlanets(id)
      .then(this.onPlanetLoaded)
      .catch(this.onError)
  };

  onRandomClose = ( close) => {
    this.setState({close:true});
  };

  onRandomOpen = ( close) => {
    this.setState({close:false,
                    loading: true});
    this.updatePlanet()
  };

  render () {

    
    const { planet, loading, error, close} = this.state;

    const errorIndicator = error ? <Error/> : null;

    const chosenSolusion = !(loading || error || close) ;
    const closeItem = !close? null : null;
    const loader =  loading ? <Loader/> : null;
    const content = chosenSolusion ? <PlanetView planet={planet}/> : null;
  
    return(
      <div>
        <div className="random_planet"> 
          {errorIndicator}
          {loader}
          {content}
          {closeItem}
        </div>
        <div className="random-planet_div">
          <button className="random-button" 
                  onClick={this.onRandomClose}>
            Close
          </button>
          <button className="random-button" 
                  onClick={this.onRandomOpen}>
            Open
          </button>
        </div>
      </div>
    );
  }
}

const PlanetView =({planet}) => {
  const { id, name, population, 
    rotationPeriod, diameter} = planet;
  return(
      <React.Fragment>
        <img src={`http://starwars-visualguide.com/assets/img/planets/${id}.jpg`} alt='' className="planet-img"/>
        <div>
          <div className="planet-name">
            {name}
          </div>
          <div className="options-random">
            <span className="option-random">
              Population: {population}
            </span>
            <span className="option-random">
              Rotation period {rotationPeriod}
            </span>
            <span className="option-random">
              Diameter {diameter}
            </span>
          </div>
        </div>
      </React.Fragment>
  )
}
