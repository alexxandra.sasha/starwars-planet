import React from 'react';
import {SwapiServiceConsumer} from "../context-helper";

const withSwapiService = (mapMethodsToProps) =>(Wrapped ) => {
  return (props) => {
    return (
      <SwapiServiceConsumer>
        {(swapiService) => {
          const serviceProps = mapMethodsToProps(swapiService);
          return (
            <Wrapped {...props} {...serviceProps}/* swapiService={swapiService} *//>
          )
        }}
      </SwapiServiceConsumer>
    )
  } 
}

export default withSwapiService;