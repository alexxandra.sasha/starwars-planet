import withData from './with-data';
import withSwapiService from "./with-swapi-service";
import {withChildrenFunction} from "./with-child-function"
import compose from "./compose"
export {withData,
  withSwapiService,
  withChildrenFunction,
  compose};

