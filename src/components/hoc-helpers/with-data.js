import React, {Component} from 'react';
import Loader from '../loader';

const withData = (View/* , getData */) => {
  return class extends Component {
  // swapiService = new SwapiService ();

  state = {
    itemList: null,
  }

  componentDidMount () {
    // this.swapiService
    // .getAllPeople() 
    // const {getData} = this.props;
    /* getData() */
    this.props.getData()
      .then((itemList) => {
        this.setState({itemList})
      }
    )
  }

    render() {
      const {itemList} = this.state;

      if (!itemList ){
        return <Loader/>
      }

      return <View {...this.props} itemList={itemList}/>
    }
  }
}

export default withData;
