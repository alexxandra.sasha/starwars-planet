import React from 'react';
import PropTypes from "prop-types";
import "./row.css";
;

const Row = ({left, right}) => {
  return (
    <div className="main_area">
        {left}
        {right}
    </div>
  )
}

Row.propTypes = {
  left: PropTypes.node.isRequired,
  right: PropTypes.node.isRequired,
}

export default Row;