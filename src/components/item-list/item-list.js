import React from 'react';
import PropTypes from "prop-types";
import "./item-list.css";

const  ItemList = (props) => {

    const {itemList, onItemsSelected, children: renderLabel  } = props;

    const items = itemList.map((item) => {
      const  {id} = item;
      const label = renderLabel(item)
/*       const label = this.props.children(item) */;
      // const id = this.props.renderId(item);
      return (
      <span className="item_list-option item_list-option-each"
            key={id}
            onClick={() => onItemsSelected(id)}
              >
        {label}
      </span>
      );
    });

    return(
      <div className="item_list"> 
       {items}
      </div>
    );
  }

  ItemList.defaultProps = {
    onItemsSelected: ()=> {}
  }

  ItemList.propTypes = {
    onItemsSelected: PropTypes.func,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    children: PropTypes.func.isRequired
  }

export default ItemList;