import React from 'react';
import ItemDetails from "../item-details";
import Record from '../record';
import {withSwapiService, withChildrenFunction, withDetails} from "../hoc-details";

const mapPersonMethodsToProps = (swapiService) => {
        return {
                getData: swapiService.getPerson,
                getImageUrl: swapiService.getPersonUrl
        }
}

const ListWithPeople = withChildrenFunction (      
        ItemDetails,
        <Record field="gender" label="Gender"/> ,
        <Record field="eye_color" label="Eye color"/>
)

const PersonDetails = withSwapiService(mapPersonMethodsToProps)(
        withDetails(ListWithPeople/* , getPerson, getPersonUrl */)
        )
        

export {PersonDetails};