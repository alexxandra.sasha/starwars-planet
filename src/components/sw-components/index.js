
import {
  PersonList,
  StarshipList,
  PlanetList} from './item-list';

import {PersonDetails} from "./person-details";
import {PlanetDetails} from "./planet-details";
import {StarshipDetails} from "./starship-details";



export {PersonDetails,
  StarshipDetails,
  PlanetDetails,
  PersonList,
  StarshipList,
  PlanetList};

