import React from 'react';
import {withData, 
        withSwapiService,
        withChildrenFunction,
        compose} from "../hoc-helpers";
import ItemList from '../item-list';

// const swapiService = new SwapiSerice();
// const {getAllPeople,
//       getAllPlanets,
//       getAllStarships,
//       } = swapiService;

const mapPersonMethodsToProps = (swapiService) => {
        return {
                getData: swapiService.getAllPeople
        }
}

const mapStarshipMethodsToProps = (swapiService) => {
        return {
                getData: swapiService.getAllStarships
        }
}

const mapPlanetMethodsToProps = (swapiService) => {
        return {
                getData: swapiService.getAllPlanets
        }
}

const render = (
        ({name}) => <span>{name}</span>
)


const PersonList = compose(
                withSwapiService(mapPersonMethodsToProps),
                withData,
                withChildrenFunction(render)
                )( ItemList );
const StarshipList = compose(
                withSwapiService(mapStarshipMethodsToProps),
                withData,
                withChildrenFunction( render)
                )(ItemList);
const PlanetList = compose(
                withSwapiService(mapPlanetMethodsToProps),
                withData,
                withChildrenFunction( render)
                )( ItemList);
                       
export {PersonList,
        StarshipList,
        PlanetList};