import React from 'react';
import ItemDetails from "../item-details";
import Record from '../record';
import {withSwapiService, withChildrenFunction, withDetails} from "../hoc-details";

const mapStarshipMethodsToProps = (swapiService) => {
        return {
                getData: swapiService.getStarships,
                getImageUrl: swapiService.getStarshipUrl
        }
}

const ListWithStarships = withChildrenFunction (      
        ItemDetails,
        <Record field="cargoCapacity" label="Cargo capacity"/>,
        <Record field="consumables" label="Consumables"/>,
)

const StarshipDetails = withSwapiService(mapStarshipMethodsToProps)(withDetails(ListWithStarships))

export {StarshipDetails};