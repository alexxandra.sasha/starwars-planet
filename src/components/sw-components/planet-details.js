import React from 'react';
import ItemDetails from "../item-details";
import Record from '../record';
import {withSwapiService, withChildrenFunction, withDetails} from "../hoc-details";

const mapPlanetMethodsToProps = (swapiService) => {
        return {
                getData: swapiService.getPlanets,
                getImageUrl: swapiService.getPlanetUrl
        }
}

const ListWithPlanets = withChildrenFunction (      
        ItemDetails,
        <Record field="population" label="Population"/>,
        <Record field="rotationPeriod" label="Rotation period"/>
)

const PlanetDetails = withSwapiService(mapPlanetMethodsToProps)(withDetails(ListWithPlanets));

export {PlanetDetails};