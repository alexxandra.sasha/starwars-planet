import React from 'react';
import "./record.css";


const Record = ({field, item, label}) => {

  return (
    <span className="option-person">
            {label} {item[field]} 
    </span>
  )
}

export default Record;