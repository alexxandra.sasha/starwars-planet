import React, {Component} from 'react';
import "./header.css";
import {Link, NavLink} from "react-router-dom";

export default class Header extends Component {

  render () {
  
    return(
      <div className="header"> 
        <div className="title">
          <NavLink to="/">Star DB</NavLink>
        </div>
        <div className="options">
          <span className="option">
            <Link to="/people">People</Link>
          </span>
          <span className="option">
            <Link to="/planets">Planets</Link>
          </span>
          <span className="option">
            <NavLink to="/starships">Starships</NavLink>
          </span>
        </div>
      </div>
    );
  }
}
