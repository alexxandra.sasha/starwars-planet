import React, {Component} from 'react';
import "./page.css";
import Error from "../error";
import Row from "../row";
import ErrorBoundry from "../error-boundry";
import { PlanetList, PlanetDetails } from '../sw-components';

export default class PlanetPage extends Component {
  
  state={
    itemId: null,
    hasError: false,
  };

  onItemsSelected=(id)=>{
    this.setState({itemId: id})
  };

  componentDidCatch(){
    console.log("error");
    this.setState({hasError:true})
  };

  render () {
    if (this.state.hasError) {
      return <Error/>
      
     
   };
    return(
      <ErrorBoundry>
        <Row left={<PlanetList onItemsSelected={this.onItemsSelected}/>} 
            right={<PlanetDetails itemId={this.state.itemId}/>}/>
      </ErrorBoundry>
    )
  }
}
