import React, {Component} from 'react';
import "./page.css";
import Error from "../error";
import SwapiService from '../../services/swapi-service';
import Row from "../row";
import ErrorBoundry from "../error-boundry";
import { StarshipDetails, StarshipList } from '../sw-components';

export default class StarshipPage extends Component {
  
  swapiService = new SwapiService();

  state={
    itemId: null,
    hasError: false,
  };

  onItemsSelected=(id)=>{
    this.setState({itemId: id})
  };

  componentDidCatch(){
    console.log("error");
    this.setState({hasError:true})
  };

  render () {
    if (this.state.hasError) {
      return <Error/>
   };

    return(
      <ErrorBoundry>
          <Row
            right={<StarshipDetails itemId={this.state.itemId}/>}
            left={<StarshipList onItemsSelected={this.onItemsSelected}/>}/>
      </ErrorBoundry>
    )
  }
}
