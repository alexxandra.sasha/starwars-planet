import React, {Component} from 'react';
import "./page.css";
import Error from "../error";
import Row from "../row";
import ErrorBoundry from "../error-boundry";
import {PersonList, PersonDetails} from "../sw-components";

export default class PeoplePage extends Component {

  state={
    itemId: null,
    hasError: false,
  };

  onItemsSelected=(itemId)=>{
    this.setState({itemId})
  };

  componentDidCatch(){
    console.log("error");
    this.setState({hasError:true})
  };

  render () {
    if (this.state.hasError) {
      return <Error/>
      
      debugger;
   };

  //  const itemList = (
  //     <ItemList onItemsSelected={this.onItemsSelected}
  //               getData={this.swapiService.getAllPeople} 
  //     // renderItem={({name, gender, birth_year }) => 
  //     //   `${name}(${gender}, ${birth_year})`}
  //     // renderId={(item) => item.id}
  //     >
  //       {(i ) => 
  //        `${i.name}(${i.gender}, ${i.birth_year})`}

  //     </ItemList>
  //  );

  //  const personDetails = (
  //   <ItemDetail itemId={this.state.itemId}/>
  //  );

    return(
      // <div className="main_area">
      //     {itemList}
      //     {personDetails}
      // </div>
      <ErrorBoundry>
        <Row left={<PersonList onItemsSelected={this.onItemsSelected}/>} 
          right={<PersonDetails itemId={this.state.itemId} />}/>
      </ErrorBoundry>


    )
  }
}
