import React, {Component} from 'react';
import "./loader.css";
;

export default class Loader extends Component {

  render () {
  
    return(
     <div className="loader">

        <div className="loadingio-spinner-double-ring-wu2xurn5b5s">
          <div className="ldio-608bx9khes4">
        <div></div>
        <div></div>
        <div><div></div></div>
        <div><div></div></div>
        </div></div>

     </div>
    )
  }
}
