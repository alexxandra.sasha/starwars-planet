import React from 'react';

const withChildrenFunction = (Wrapper, fn1, fn2) => {
        return (props) => {
                return (
                        <Wrapper {...props}>
                                {fn1}
                                {fn2}
                        </Wrapper>
                )
        }
}


export {withChildrenFunction};