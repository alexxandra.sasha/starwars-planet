import withDetails from './with-details';
import withSwapiService from "./with-swapi-service";
import {withChildrenFunction} from "./with-child-function";
import compose from "./compose";

export {withDetails,
        withSwapiService,
        withChildrenFunction,
        compose};

