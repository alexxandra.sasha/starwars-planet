import React, {Component} from 'react';
import Loader from "../loader";

const withDetails = (Details/* , getData, getImageUrl */) => {
  return class extends Component {

  state = {
    item: {},
    loading: true,
    image: null,
  }

  componentDidMount(){
    this.updateData();
  };

  componentDidUpdate(prevProps){
    if(this.props.itemId !== prevProps.itemId){
      return this.updateData();
    }
  }

  updateData(){
    const {itemId} = this.props;
    if(!itemId){
      return;
    } else {
      this.setState({loading:true})
    }

/*     this.swapiService
      .getPerson(itemId) */
      /* getData(itemId) */
      this.props.getData(itemId)
      .then((item) => {
        this.setState({item,
          loading:false,
          image: this.props.getImageUrl(item),
        })
    })
  } 
  // constructor(){
  //   super();
  //   this.updatePeople()
  // }

  // onPeopleLoaded=(people)=>{
  //   this.setState({people})
  // }

  // updatePeople () {
  //   const id = Math.floor(Math.random()*25) + 2
  //   this.swapiService
  //     .getPerson(id)
  //     .then(this.onPeopleLoaded)
  // }

    render () {
      const {  item, loading, image } = this.state;
      
      if (loading) {
        return <Loader/>
      }
      return <Details {...this.props} item={item} image={image} />
    }
  }
}
export default withDetails;