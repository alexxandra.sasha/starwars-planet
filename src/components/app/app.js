import React, {Component} from 'react';
import "./app.css";
import Header from '../header';
import RandomPlanet from '../random-planet';
import ErrorButton from "../error-button";
import Error from "../error";
import PeoplePage from "../page";
import SwapiService from '../../services/swapi-service';
import ErrorBoundry from '../error-boundry';
import {SwapiServiceProvider} from "../context-helper";
import PlanetPage from '../page/planet-page';
import StarshipPage from '../page/starship-page';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import { StarshipDetails } from '../sw-components';

export default class App extends Component {

  swapiService = new SwapiService ();

  state={
    itemId: null,
    hasError: false
  };

  onItemsSelected=(id)=>{
    this.setState({itemId: id})
  }

  componentDidCatch(){
    console.log("error");
    this.setState({hasError:true})
  }
  
  render () {

    // const {getPerson, getStarships, getPlanets,
    //       getPersonUrl, getPlanetUrl, getStarshipUrl} = this.swapiService;

    if (this.state.hasError) {
       return <Error/>
    };

    // const starshipDetails = (
    //   <ItemDetails itemId={10}
    //                getData={getStarships}
    //                getImageUrl={getStarshipUrl}>
    //               <Record field="cargoCapacity" label="Cargo capacity"/>
    //               <Record field="consumables" label="Consumables"/>
    //               <Record field="created" label="Created"/>
    //   </ItemDetails>
    // )
    return(
      <ErrorBoundry>
        <SwapiServiceProvider value={this.swapiService}>
          <BrowserRouter>
                <Header/>
                <RandomPlanet /> 
                <ErrorButton/> 
        {/*          <div className="main_area">
                  <ItemList onItemsSelected={this.onItemsSelected} />
                  <PersonDetails itemId={this.state.itemId}/>
                </div> */}
                {/* <div className="main_area">
                  <ItemList onItemsSelected={this.onItemsSelected} 
                            getData={this.swapiService.getAllStarships}
                            renderItem={(item) => item.name}
                            renderId={(item) => item.id}/>
                  <PersonDetails itemId={this.state.itemId}/>
                </div> */}
              <Routes>
                <Route exact path="/" 
                      render={()=><Header/>} 
                      element={<div>Welcome to StarDB</div>}
                      />
                <Route path="/people" element={<PeoplePage/>} />
                <Route path="/planets" element={<PlanetPage/>}/>
                <Route exact path="/starships"  element={<StarshipPage/>}/>
                <Route path="/starships/:id" 
                        element={<StarshipDetails />}
                        // children={({match})=> {
                        //   console.log(match)
                        //   const {id} = match.params;
                        //   return <StarshipDetails itemId={id}/>
                        // }}
                        />
              </Routes> 

                

              {/* <ItemList onItemsSelected={() => {}}
                        // getData={this.swapiService.getAllPeople} 
              // renderItem={({name, gender, birth_year }) => 
              //   `${name}(${gender}, ${birth_year})`}
              // renderId={(item) => item.id}
              >
                {(item ) => 
                `${item.name}`}

              </ItemList> */}
            
     
          </BrowserRouter>
        </SwapiServiceProvider>

      </ErrorBoundry> 
    );
  }
}