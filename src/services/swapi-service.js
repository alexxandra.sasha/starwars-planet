
export default class SwapiService {

  _apiBase = "https://swapi.py4e.com/api";
  _imageBase = "http://starwars-visualguide.com/assets/img/"

  getResource = async (url) => {
    const res = await fetch(`${this._apiBase}${url}`);
    if(!res.ok){
      throw new Error(`could noew fetch ${url}` + 
        `, received ${res.status}`)
    }
    return await res.json();
  }

  getAllPeople = async () =>{
    const res = await this.getResource(`/people/`);
    return res.results.map(this._transformPerson);
  }

  getPerson = async (id) => {
    const people = await this.getResource(`/people/${id}`)
    return this._transformPerson(people)
  }

  getAllPlanets = async () => {
    const res = await this.getResource(`/planets/`);
    return res.results.map(this._transformPlanet);
  }

  getPlanets = async (id) => {
    const planet = await this.getResource(`/planets/${id}`);
    // return this.getResource(`/planets/${id}/`)
    return this._transformPlanet(planet);
  }

  getAllStarships = async () => {
    const res = await this.getResource(`/starships`);
    return res.results.map(this._transformStarship);
  }

  getStarships = async (id) => {
    const starships = await this.getResource(`/starships/${id}`)
    return this._transformStarship(starships)
  }

  getPersonUrl = ({id}) => {
    return `${this._imageBase}characters/${id}.jpg`}

  getPlanetUrl = ({id}) => {
    return `${this._imageBase}planets/${id}.jpg`}

  getStarshipUrl = ({id}) => {
    return `${this._imageBase}starships/${id}.jpg`}

  _extractId = (item) =>{
    const idRegEx = /\/([0-9]*)\/$/;
    return item.url.match(idRegEx)[1];
  }
  
  _transformPlanet = (planet) => {
    return {
      id: this._extractId(planet),
      name: planet.name,
      population: planet.population,
      rotationPeriod: planet.rotation_period,
      diameter: planet.diameter
    } 
  }

  _transformPerson = (people) => {
    return {
      id: this._extractId(people),
      name: people.name,
      gender: people.gender,
      birth_year: people.birth_year,
      eye_color: people.eye_color
    }
  }

  _transformStarship = (starships) => {
    return {
      id: this._extractId(starships),
      name: starships.name,
      cargoCapacity: starships.cargo_capacity,
      consumables: starships.consumables,
      created: starships.created,
    }
  }

}



// const swapi = new SwapiService();

// swapi.getStarships(3).then((p)=> {
//   console.log(p.name)
// })
// const getResource = async(url) => {
//   const res = await fetch(url);
//   if(!res.ok){
//     throw new Error(`could noew fetch ${url}` + `, received ${res.status}`)
//   }

//   const body = await res.json();
//   return body;
// }

// getResource("http://swapi.dev/api/people/100000")
//   .then((body) => {
//     console.log(body)
//   })
//   .then((err)=>{
//     console.error(err);
//   })

// fetch("http://swapi.dev/api/people/1")
//   .then ((res) => {
//     return res.json();
//   })
//   .then ((body) => {

//     console.log(body)
//   })